#!/usr/bin/env python3
import config
import asyncio
import websockets

async def client(data_query):
    uri = "ws://{ip}:{port}".format(ip=config.ws_ip, port=config.ws_port)
    async with websockets.connect(uri) as websocket:
        # monitor_type = input("monitor_type: [monitor,equipment]")
        # options = set([inverter_interface_definitions.register_map[register][monitor_type]
        #            for register in inverter_interface_definitions.register_map])
        # print(options)
        # monitor_options = input("monitor_options: ")
        # data = "{0}:{1}".format(monitor_type,monitor_options)
        # Send username as JSON object to server
        await websocket.send(data_query)

        response = await websocket.recv()
        print(response)


inverter_file = config.model
inverter_interface_definitions= __import__(inverter_file)
while True:
    data = input("query? ")
    if not data:
        break
    elif data.find(':') >= 0:
        data_query = data
        monitor_type, monitor_options = data.split(':')
        monitor_type = monitor_type.strip()
        monitor_options = monitor_options.strip()
        monitor_group = [register for register in inverter_interface_definitions.register_map if
                         inverter_interface_definitions.register_map[register][monitor_type] == monitor_options]
    else:
        data_query = ''
        for q in data.split():
            if q in inverter_interface_definitions.register_map:
                data_query += ' ' + q
        monitor_group = data_query
    print(data_query, monitor_group)
    asyncio.get_event_loop().run_until_complete(client(data_query))