# ip i port de comunicació amb l'inversor
inverter_ip = "192.168.1.97"
inverter_port = 502
timeout = 3
# En principi no cal canviar. ip i port de comunicació amb al eos_inverter.py. A nodered s'han d'usar els mateixos.
ws_ip = "0.0.0.0" # equival a localhost
ws_port = "8765"
# No canviar. L'inversor Sun2000l només respon unit_id zero.
slave = 0x00  # unit_id
# No canviar. És un punter a Huawei.py
model = "Huawei"