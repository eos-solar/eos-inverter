# !/usr/bin/python3

import config
from modbustcp import connect_bus, read_registers, close_bus
import time
import asyncio
import websockets
import logging
import json

_logger = logging.getLogger(__name__)


def to_str(s):
    str_return = ""
    for i in range(0, len(s)):
        high, low = divmod(s[i], 0x100)
        if high != 0:
            str_return = str_return + chr(high)
        if low != 0:
            str_return = str_return + chr(low)
    return str_return


def to_U16(i):
    return i[0] & 0xffff


def to_I16(i):
    i = i[0] & 0xffff
    return (i ^ 0x8000) - 0x8000


def to_U32(i):
    return (i[0] << 16) + i[1]


def to_I32(i):
    i = ((i[0] << 16) + i[1])
    i = i & 0xffffffff
    return (i ^ 0x80000000) - 0x80000000


def to_Bit16(i):
    return i[0]


def to_Bit32(i):
    return (i[0] << 16) + i[1]


def call_function(method_name, values):

    method_name = "to_" + method_name
    possibles = globals().copy()
    possibles.update(locals())
    method = possibles.get(method_name)
    if not method:
        return 'no_method_error', None
    try:
        return '', method(values)
    except:
        return "error in call_function".format(method_name, values), None


async def read_inverter(websocket, path):

    def get_registers(interface_definitions, definition_group):

        get_error = False

        conn = connect_bus(ip=config.inverter_ip,
                           port=config.inverter_port,
                           timeout=config.timeout)
        out = ""
        values = []
        values_new = {}
        if conn:
            time.sleep(0.5)
            for register in definition_group:
                result = None
                k = 0
                while True:
                    try:
                        result = read_registers(conn, config.slave, interface_definitions.register_map[register])
                        break
                    except:
                        if result == -1:
                            time.sleep(0.3)
                            k += 1
                            if k > 3:
                                break
                        else:
                            raise

                if result == -1:
                    get_error = 'Read register error'
                    break

                get_error, value = call_function(interface_definitions.register_map[register]['type'], result.registers)
                if get_error:
                    break
                if (interface_definitions.register_map[register]['type'] != 'str') and (
                        interface_definitions.register_map[register]['scale'] != 1):
                    value = value / interface_definitions.register_map[register]['scale']

                if interface_definitions.register_map[register]['units'] == 's':
                    if value > 2600000000:
                        continue
                    value = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(value))
                elif interface_definitions.register_map[register]['use'] == "stat":
                    value = interface_definitions.status(register, value)
                if register == 'Time':
                    value = value.split()[1]
                values.append('"{register}":"{val}"'.format(register=register, val=value))
                values_new[register] = {'value': value,
                                        'mesurement': interface_definitions.register_map[register]['mesurement'],
                                        'fieldname': interface_definitions.register_map[register]['fieldname']
                                        }
            close_bus(conn)
            if not get_error:
                out = '{' + ','.join(values) + '}'
                _logger.debug(out)
        else:
            get_error = 'Connection error'

        return get_error, out, values_new

    options = await websocket.recv()
    inverter_file = config.model
    inverter_interface_definitions = __import__(inverter_file)
    print('reading... ', options)
    if options.find(':') >= 0:
        monitor_type, monitor_options = options.split(':')
        monitor_type = monitor_type.strip()
        monitor_options = monitor_options.strip()
        monitor_group = [register for register in inverter_interface_definitions.register_map if inverter_interface_definitions.register_map[register][monitor_type] == monitor_options]
    else:
        monitor_group = options.split()
    retry = 3
    j = 0
    while j < retry:
        error, registers, registers_mesure = get_registers(inverter_interface_definitions, monitor_group)
        if error:
            _logger.error(error)
            time.sleep(2)
            j += 1
        else:
            break
    if j == retry:
        _logger.info("Unable to get results")
    elif registers:
        await websocket.send(json.dumps(registers_mesure))
        # print(f"> {registers}")
        # print(f"> {registers_mesure}")

if __name__ == '__main__':
    '''Usage
    
    Group query:
        send string "<monitor_type>:<monitor_option>"
            <monitor_type> can be 'mesurement' o 'monitor'
            <monitor_option> se Huawei.py
        Example:
            "mesurement:optimizer" will return register values of Optim_tot, Optim_on, Optim_opt
            "monitor:info will return register values of Temp, P_acum, P_daily, M_PTot 
            
        Websocket is opened and closed once for every group query
        
    Individual register:
        send string with register name
        Example: 
            "P_peak" will return Peak Power
            
    Multiple registers
        send a <space> separeted string with list of registers
        Example:
            "P_peak P_accum M_PTot"
         
    Return values are in json format
    '''
    start_server = websockets.serve(read_inverter, config.ws_ip, config.ws_port)
    print("Server started",config.ws_ip, config.ws_port)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_forever()

